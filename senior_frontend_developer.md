<img src="https://gitlab.com/sticky3/jobs/-/raw/main/we_are_hiring.jpeg" width="500px" />

# Senior frontend developer

## About Us:
* Sticky aims to be the leading worldwide mobile-native token marketplace.
* Creators & collectors on Sticky get the fun of NFTs and other non-currency tokens in a fun & inviting app, with the safety of the App Store, without the hassle of setting up crypto wallets.
* Sticky has a private ledger and is in the process of integrating with Ethereum, Polygon and Solana. 
* Launched in early 2022 by experienced founders, it has received early-stage backing from top VC investors and is now building an all-star, remote-first team.
* iOS: https://apps.apple.com/us/app/nft-marketplace-maker-sticky/id1497044357
* Web: https://getstic.ky
 
## This job:
* You will be responsible for creating responsive web pages based on complete prototypes and designs; planning and coding nice transitions and experiences for each user interaction, be attentive to details of the layout in order to fully convert them; developing web pages considering performance and user experience in all browsers and devices; participating in code reviews in order to contribute to the team’s development, including yours.

 
## You need to have:
* Proficiency in HTML, CSS, Javascript, React and Next.js;
* Have experience with front-end development consuming REST APIs;
* Security, coding & version control best practices;
* Basic English skills to read documentation & communicate with the rest of the team;
* Interest in working on the Sticky platform.

## If you have these too that's nice:
* Full stack web development expertise;
* Interest or experience with blockchain, cryptocurrencies, NFTs;
* Entrepreneurial experience (e.g. a startup or side project of your own);
* Early-stage startup experience ;
* Strong academic credentials (e.g. top university, degree in Computer Science / Engineering);
* Practical experience with automated testing development.

## We offer:
* Blood, toil, tears and sweat;
* Generous salary and stock options;
* The opportunity to build and grow with a cutting-edge VC-backed global marketplace from the start, as one of its first 10 developers;
* An entrepreneurial, pragmatic, high performance, transparent and friendly culture.
 
## Place:
* Wherever you want (fully remote)

## How to apply
If you believe that you are a good fit for this job, please contact us and send your updated resume, LinkedIn profile and salary expectation to talent@getstic.ky
