<img src="https://gitlab.com/sticky3/jobs/-/raw/main/we_are_hiring.jpeg" width="500px" />

# CTO

---

## About Us:

- Sticky is the mobile-native token marketplace.
- Creators & collectors on Sticky get the fun of NFTs and other non-currency tokens in a fun & inviting app without the hassle of setting up crypto wallets.
- Launched in early 2022 by experienced founders, it has received early-stage backing from top VC investors and is now building an all-star, remote-first team.
- iOS: https://apps.apple.com/us/app/nft-marketplace-maker-sticky/id1497044357
- Web: https://getstic.ky

## This job:

- You will answer directly to the Founder/CEO and will be responsible for all technical aspects of the Sticky token marketplace.

## You need to:

- Have multi-year experience leading teams developing successful B2C apps that operate on multiple platforms (iOS, Android, web) from an early stage.
- Have a strong network of technology professionals to recruit from.
- Speak fluent English.
- Find the Sticky app an exciting product to work on.
- Are interested in blockchains, digital assets, and marketplaces in general.
- Believe you have what it takes to develop the Sticky app into a global leader with tens of millions of creators and collectors.

## If you have these too that's nice:

- Experience working with NFT or other blockchain products.
- Entrepreneurial experience.
- Experience at other early-stage startups.

## We offer:

- Blood, toil, tears, and sweat.
- Unique opportunity to lead global product in VC-backed company.
- Generous salary and stock options.
- A place where you can share ideas with other outstanding professionals.
- A culture that encourages freedom and self-development.

## Place:

- Wherever you want (fully remote)